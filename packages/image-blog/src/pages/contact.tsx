import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Contact from "../containers/Contact"

type ContactPageProps = {}

const ContactPage: React.FunctionComponent<ContactPageProps> = () => {
  return (
    <Layout>
      <SEO title="Contact Us" description="PinArtWork Contact Page" />

      <Contact />
    </Layout>
  )
}

export default ContactPage
