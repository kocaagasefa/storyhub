---
title: How to fall asleep for a kid ?
date: '2020-04-12T23:34:37.121Z'
description: How to fall asleep for a kid ?
cover: './preview.jpeg'
tags:
  [
    'how to fall asleep for a kid',
    'how to fall asleep for a kid fast',
    'featured',
  ]
---

**First of all, I think the importance of sleep in children should be mentioned.**

If you do not explain the importance and effects of sleep to your child, if you do not persuade your child, you will not be able to make them sleep.

So what do you need to talk about sleeping with them?

You can explain the issues detailed below in a language that they can understand.

Children who do not sleep well are more prone to obesity and depression and increases memory and learning capability.

**Main Importance of Sleep**

Sleep is not just a means of rest. Sleep is essential for the regeneration of all organs, especially the brain. During sleep, stress hormones decrease while growth hormone increases. In this way, the body repairs itself, restructures, protein synthesis increases during sleep and the body prepares itself for the new day. The younger the child, the faster the growth and the need for sleep. Growth will be faster in a regular sleeping. It was also revealed that learning capability and memory were stronger in these children. The situation is the opposite of children who are irregular sleeping. Obesity was found to be more common in children who could not sleep enough. These children have more time to eat junk food, drink excessive calorie drinks, and spend time in front of the television or computer.

Scientific research has shown that regular sleep in preschool children reduces hyperactivity, anxiety, and depression. Again, it has been shown that half of the day (which means at least 11-12 hours of sleep per day) is necessary for children within at least 3-4 days of every week. It has been shown that the tendency to depression is higher in children who are satisfied with 7-7.5 hours of sleep daily.

The frequency of depression in these children was shown to be 42% higher than those who went to bed before 10:00 pm. While ideal sleep time is 9 hours for teens, many children are satisfied with 7-7.5 hours of sleep.

It should be ensured that the time to fall asleep throughout the entire childhood is before 10:00 pm. It may not always be easy to achieve this. Establishing a healthy sleep habit should be started from early infancy.

**The Effect of Nutrition Factors in Sleep Quality of Children**

There are also some nutritional factors in sleep. Lowering blood sugar in the middle of the night will lead to waking up. In this respect, it is necessary to avoid carbohydrate-rich foods (bakery and sugar) close to sleep time. Food allergies will also cause itching, asthma and digestive problems, disrupting sleep quality. Removing cow milk from the diet provides an improvement in sleep quality in a child with cow's milk allergy. Also, food allergies will lead to the growth of tonsils and adolescents in children, leading to sleep problems. Low intake of calcium, magnesium and B vitamins (especially B6) increases night awakenings. Caffeinated foods taken before bedtime (lots of drinks and chocolates) can cause insomnia due to its stimulating effect.

Sleep is as important as nutrition in children's development. Uninterrupted and quality sleep is very important for the mental and physical development of babies. If you teach a child to sleep properly, he will love sleep and will not cause any difficulties. All he needs to know is how to sleep.

**Here are lots of tips to learn how to get your kids to sleep**

A regular sleeping hour, a pre-sleep tale and relaxing music, providing opportunities for bodily activity during the day, offering a list of attractive activities and surprises waiting for it when awakened, leaving the child alone with a trust object (a blanket or toy to love), almond oil, Massage with chamomile or lavender oil can make sleep sweet for the child.

Creating an environment of peacebuilding, not fight, between the family and the child about the time to go to sleep will perhaps provide a solution to the problems.

Sleep is a vital requirement for child health and growth. It supports sleep, wakefulness and memory performances. Adequate sleep for children helps to increase children's functions, reduce behavioral problems and prevent moodiness. That is why it is so important to teach your child to sleep and help him develop early.

Learning and practicing sleep habits well for children can be the first step to solve sleep problems that may arise in later ages, so getting a good sleep habit is a very important attitude.

**How much sleep do children need?**

"How long should my child sleep?" The answer to the question varies according to the age of the child. Here is the child's need for sleep by months and ages.
![How much sleep do children need](https://sleepico.com/img/How-much-sleep-do-children-need.png)

These sleeping hours should also be periodically within 24 hours. It is important to keep track of how much your child sleeps, at least as important as the hour of sleep, a suggestion to help develop a consistency for the sleep routine. If your child sleeps 4 hours on 3 days a week and then sleeps 14 hours for 4 days, your child may have sleep disturbance.

To develop your child's sleep habits, you should routinely follow your child every night.

When it's time to sleep, finding and practicing a sleep time ritual will help your child relax and rest and sleep easily during the night.

**Typical sleep time routines**

Take a bath,

Dress in pajamas,

Brush your teeth,

Read a story,

Make sure the room is comfortable and quiet and the temperature is sufficient,

Put your child on his bed,

Say good night and leave.

Some children are congenital good sleepers. Some need to be trained. So, what is the secret of smart mothers who do this job successfully?

Here are 5 of the most common applications to get your kids sleep.

**1. They are strict about sleep routine**

They send the guests after the meal so that their children can sleep on time. Until their children are at least 9 years old, they are not gone anywhere at night. These mothers abandon many social opportunities to get their children to bed on time. Sleeping early is better for the child's rhythm and healthy sleep habits.

**2. Sleep only in the bed**

These mothers do their best to ensure that their children sleep only in their beds, not in the strollers.

**3. They care about sleep education**

Sleep education does not end by teaching children to fall asleep. It is also important to teach them how to stay awake - or at least stay in their rooms and play quietly until they sleep.

**4. They shut down all electronic devices at least one hour before bedtime**

We know that the light coming from the screens disrupts the daily rhythms of adults. For children, these effects are many times higher. Before the bed fight begins, you have to turn off all electronic devices.

**5. They make the room cool and quiet**

Is the room temperature 20 ° C even in the winter season?

All the devices are turned off?

Are the curtains closed well?

Is a favorable environment for sleeping completely provided?

**How to make rules against excuses of our children to make them sleep with Case Study?**

Studies show that, unfortunately, our children are not sleeping enough and they lie down very late. Unfortunately, most of the children in the 3-5 age range go to bed after 10:00 pm, which is a late hour for them. Many parents do not know how to deal with their child's sleep resistances after 3 years of age, and they can be deadlocked. It is becoming more challenging to sleep, and it may be taking hours. The most important thing to do is to be patient and consistent.

Children are very smart. They know what their demands are to be fulfilled, they concentrate on the "I want another glass of water", "Tell me another tale", "Mother, you go, my father, come", "Cover me", "I will go to the toilet", "Scrape my back", "A lullaby say more ”,“ I have to say something to your ear ”, it can continue until the excuses end at night and break your sleep and turn your night into a nightmare. Parents have to control our children, if this is not the case in your home, things should change. If the boss has a small child at home, it is time for things to change. Children should not be allowed to insist on unnecessary things about sleep. If you give them this power, they will use it. The situation quickly gets out of your control.

I know that the resistance of children to go to bed during sleep times is challenging for many parents. Especially children between the ages of 2-6 make up excuses for not going to bed. Endless sentences like “Mom, I'm watching TV a little more, dad tells another fairy tale, I'll go to the toilet, I'm thirsty, Can I sleep here ?” So what we have to do is; determining rules about early bedtime and maintaining these rules steadily.

If the first half of the solution is to set these rules, the other half can continue with them. The hardest part of the job starts at that point

The “a little more television” that your children offer you stands in a place you should especially reject. The lights emitted from smart devices such as computers, televisions or tablets-phones create the perception of “day is not over yet” in the brain and reprograms the sleep time. Thus, sleeplessness begins until late hours. If you put it to sleep after reading a story or a fairy tale instead of going to sleep after watching TV, you will make the development to sleep healthier.

You can do this by getting the sleep time early by pulling it back every 10 or 20 minutes every day. At the end of a week, you will see how big such changes have made for both your baby and you. In this way, changing the sleep time will take a long time, so you can spend this process calmly by accepting that your child will be able to sleep in the first time to sleep and that he will not be able to sleep in the first days. There are points that both alternatives will push. But it is worth trying!?

Sleep problems are common in infancy, early childhood, school-age and adolescence. While various sleep problems are defined in approximately 25-50 percent of preschool children, it is reported that approximately 20-30 percent of school-age children and adolescents experience problems that can be called sleep disorders. It is the most common problem that pediatricians encounter in this age group in child controls.

Parent needs to set limits or the parents should make arrangements. We see sleep disorders in some mental illnesses, but even if there is no mental illness, we see the most stress-related sleep disorder problem in children. After a stressful life event, we see sleep changes in children and at night. Medical problems such as reflux, respiratory obstruction, asthma, and allergies may cause sleep disorders at the basis of sleep disorders. The problem of sleep in children is important if sleep problems are not eliminated, that is, when the child is not able to sleep enough, there may be problems in attention processes in learning processes. There may be problems in brain development and daytime sleepiness may be experienced.

**Case Study** **I**, 4-year-old Amanda has not learned to sleep properly since the day she was born, she has always been put to sleep with the support of her mother and says she does not like sleep. All her control of sleep has been taken over, and at bedtime she makes the house do whatever she wants. When the mother got the answer to sleep together, Amanda starts saying “I should drink a glass of water.” When it comes to water, “No, this is not my glass, I don't drink from it.” Then continues like “ I want to go to the toilet, etc.”

The whole rule-maker here is Amanda.

**Case Study I Solution,** Parents have to set appropriate limits and it is imperative to be consistent with these rules. However, sometimes parents can not bear the insistence and crying of the child and feel guilt and give the child control. If we abide by the rules, the child will stop crying and choose to sleep. If we do whatever they want to make our child sleep in a short time and do not put limitations, we will probably encounter sleep problems in the long term and we may find ourselves in an inexorable situation. Of course, setting rules is hard work. Behaving too hard is just as dangerous as being too soft. Choose the type of behavior that suits your child and do not change it day or night or day. Some parents make easy rules, while others succeed during the day, but give up quickly because they are tired at night and want to sleep as soon as possible. It should not be forgotten that setting rules is a part of raising children and you will protect and raise your child in this way. Your child will somehow understand the right rules for their benefits and they will know that you are doing it for them and that this is part of your love.

**How to approach our children to make it easier to sleep them ?**

So how should we approach our children in this age group? It is imperative to find a common way with them and to satisfy both parties about sleep. First of all, we should make quality sharing with our children before sleep, especially if you are a working parent, we should have mutual sharing before sleep. You can make a jigsaw that you love together, paint the newly bought coloring book with it with great pleasure, or you can play with the family to find a card (same card match game). Thus, we have directed our child to slow activities before sleep and we can make it easier to go to sleep. Our second stage will be the start of our sleep routines, for this, the teeth are brushed, the face is washed, mentioned above.

Especially after the age of 3, our child is now an individual and needs to be asked the idea of ​​caring. We can put two different pajamas on your bed and ask which one you want to wear today.

We can read the storybook that our child chooses and listen to relaxing sleep preparatory music while preparing. But after entering the bed, our story must have been finished and of course in music. You can be with your child until he sleeps, or you can come back to check it from time to time, and give confidence and leave the room. If you refuse to sleep and come out of your room and come to you, never take it to your bed or let it spend time with you. It is now the sleep time and we must teach that they should sleep. If the child stays in bed and the correct approaches are applied, he/she will learn and continue to sleep over time.

In all the conversations about sleep, firstly, how many hours children should sleep. Cycles of Sleep In our article, we emphasized that many components of sleep are interconnected and that sleep is more active than a passive process and pointed out the importance of them. If we add information about the biological rhythms of children to this information, it is an indisputable fact that the children who lie early will be healthier and happier!

**Put your kids to bed early to make them ...**

Many studies show that children who sleep early will have better physical and emotional development. Because it sleeps earlier, the deep sleep phase is also extended. At this stage, I would like to remind you that the body is resting, repairing itself and the organisms are renewed. Therefore, the growth process will be healthier.

In a study conducted with babies older than 18 months, it was found that children lying late were more at risk of language development. In another study published in the Journal of Pediatrics, he found a direct relationship between regular sleep and emotional stability in children aged 7-11. Late bedtime can negatively affect the energy, concentration, memory and, consequently, the performance of your school-age child.

**What are the sleep problems in children?**

**1. Nightmares**

One of the common sleep disorders in children are nightmares. Nightmares are most common between the ages of 3-5, but can be seen in all age groups. Nightmares are dreamy and frightening dreams. There are two periods of sleep, one is when we dream and the brain development is faster, and the other is a heavy and restful sleep. We see the nightmares when we dream, there are important points. A child wakes up completely after seeing a nightmare and can immediately remember the details, especially after waking up. Sometimes we may not remember the nightmares that we saw in the night's sleep clearly in the morning, but when the nightmare wakes up immediately, there is a recall. A lot of nightmares can be seen in stressful and anxious periods. There is no cure for this, but what we see most often in cases is that control of stimuli is important, for example, when children watch inappropriate cartoons, they play video games that contain fear or after horror stories, nightmares can be seen frequently, first we need to bring them back to normal and then see if they continue.

**2. Night Terror-Sleep Terror**

Asleep disorder we often see after nightmares is called night terror or sleep terror. Here, the child opens his eyes in fear and sits inside the bed, crying and shouting about two or three hours after sleeping. Here, the child is asleep and especially asleep. Parents think they are awake because the eyes are open in the sleep terror experienced during the sleep period where rest is provided. If the child is not awakened at this time and is laid again, he will not remember this situation in the morning because he is asleep. These are sleep disorders that can last from 30 seconds to 3 minutes. It is important not to wake the child at this time, but of course, the parent can wake the child because he is concerned. Tiredness can occur in a child who experiences this situation, so we can recommend daytime sleep.

**3. Sleepwalking**

Sleepwalking is among the sleep disorders seen in children. Sleepwalking is one of the most important problems, we generally see 15% of children. The frequency of sleepwalking is important, we expect it to start around 4-8 years of age and decrease towards puberty. In sleepwalking, the eyes are still open, but there are dull and sedentary looks, and since he is in a heavy sleep period, if he is not awakened, he will not remember them in the morning. Safety precautions are very important because they can continue their action during sleep, for example, they can open the street door or go through the window and think of the street door and fall from pine. Unfortunately, we can see such events, the child can think of going to the toilet and urinate somewhere else in the house. Here it is important to remove the window handles, lock the doors and put the keys in a secret place, if there are rooms with steps, to keep their doors locked. Again, as in night terror, we can recommend daytime sleepiness, as there may be excessive fatigue in sleepwalking. Detailed assessment is important when sleepwalking recurs frequently. Apart from daytime sleep, programmed wake-up can also be performed in children with sleepwalking problems. If the child experiences sleep terrorism and sleepwalking at certain times every night, you can eliminate these attacks if you wake the child 15 minutes or half an hour before the hour arrives.

**3. Obstructive sleep apnea**

Apnea is an event where breathing stops temporarily. If there is any disease that obstructs the child's respiratory tract during sleep, or if there is a condition arising from the brain, a state in which breathing, which we call apnea, stops temporarily during sleep. It is generally seen in the growth of tonsils and juvenile flesh in children, but it may affect the brain development very negatively since there are minutes when oxygen is not delivered to the brain where breathing stops. In the preschool period, night wetting may occur with this sleep apnea.

**4. Restless leg syndrome**

Another problem encountered during sleep is restless leg syndrome. While this situation does not cause much trouble during the day, it occurs during sleep or at rest. Pain in the legs is a feeling of restlessness such as tingling, in mild ones, this problem disappears when moving the leg, but in severe and severe cases, the movement is not disturbed by movement. It is often seen in leg movements during sleep. There may be underlying medical conditions in restless legs syndrome. For example, we see iron deficiency 5 times more in children, we can also see kidney disease in B12 deficiency, and excessive coffee and cigarette consumption may also cause restless leg syndrome.

**5. Narcolepsy**

An important sleep disorder is what we call narcolepsy in the medical language, and sudden sleep attacks during the day are awake. Sometimes the person falls asleep while talking or eating, sometimes accompanied by falling to the floor while standing, or laughs, or suddenly with the burden of emotion, for example, when he is very happy or very upset, suddenly falling to the ground. Narcolepsy is important because the frequency of these daytime sleep attacks negatively affects a person's daily life quality. Serious damage can also occur by hitting his head when he falls suddenly.

**How to solve the sleeping problem in children?**

Sleep cycles, duration and importance and method of self-sleep,

Baby clothes, bed and blanket are made of wool,

To leave the baby awake in his bed and to learn to sleep by himself,

Baby's learning to sleep without using a tool like swinging, sucking,

Not changing the place after sleeping,

Allowing the baby to sleep with a favorite object (teddy bear, baby, cheesecloth, blanket, etc.) while being laid on the bed,

Determining the sleeping hours by the family and not compromising,

Family's determination about sleeping hours and setting appropriate limits for children,

Organizing activities, sleep ceremonies (such as telling a fairy tale, lullabies), where you can spend time with the calm and family before sleep,

Not to be laid in bed hungry, giving food or drink that will keep it light and full,

Keeping away from foods that have night-stimulating properties (coffee, tea, chocolate),

Taking care to ensure that the room is not too dark, the humidity and temperature of the environment (18 (C) is sufficient, the room is ventilated and no smoking in the room,

No electric blanket or hot water bag in the baby bed,

Failure to sleep in direct sunlight or near a fire or heater.

Often, in response to the baby crying during night awakenings, the presence of the parents with the child reduces his anxiety. In response to the child's crying, it is recommended that the time to be with him is gradually extended (desensitization). It is aimed to change the pre-sleep relationships to regulate the pre-sleep life. Individualized bed habits such as reading, singing, playing calmly, and parents should encourage them to stay in bed when the child wakes up.

Parents can do this by sitting next to the child's bed, touching him or lying next to him. If the problem of separation at bedtime is resolved, the problem of waking up at night will likely disappear.

If your baby is having trouble sleeping every night and this situation is overwhelming, do not be afraid to try our methods. Baby and mother may have trouble getting used to each other at first, this is very normal. However, remember that together with the baby having trouble falling asleep every night, mothers' sleep patterns may be disturbed and may not show the necessary attention to their baby. So what are the easy sleep of your baby? Here we shared the ways to make your baby easy to sleep.

**Warm bath**

Who wouldn't the warm touches with warm water and a soft sponge relax? Bathing will relax your baby before sleep.

**Sleeping together**

You can be with or against sleeping together, but according to the researches, children who sleep with their parents have higher self-confidence and less anxiety. Place the cradle or bed next to your bed to make it safe to sleep together. Sleeping with the baby in the same bed can be dangerous.

**Nice smell**

Some babies love fragrances. You can put a couple of drops of lavender oil on a paper napkin near the bed. Although lavender has soothing properties, it is recommended not to use odors in babies under 6 months. In babies with allergies, take care to use unscented detergent.

**A safe bed**

Keep everything inside the bed out while the baby sleeps. A blanket, toy, and even a protector around the bed can cause the baby to suffocate. A stretched rubberized bed sheet is all it takes to be in bed. If you are worried about getting cold, choose to wear a sleeping bag.

**Reflux**

The reason for some babies' sleep problems may be reflux. Acid coming out of the esophagus disturbs the baby. Vomiting, colic-style crying, sucking problems or symptoms of clogging reflux while sucking. You should immediately share your concerns with your doctor.

**Tap**

Put your baby on his bed slowly. Gently touch the belly, arm, and head with your hands. This touch will make him feel like you are close to him, making him sleep comfortably.

**Pyjamas**

Although colorful printed pajamas are very cute, they sometimes cause discomfort to the baby because they are synthetic. Prefer cotton as much as possible in baby and child pajamas. For babies' safety, try to wear pajamas that fit well on them.

**Ideal temperature**

For us to sleep comfortably, the room should not be too hot or too cold. The same rule applies to babies. The ideal room temperature for the baby room is 18-21 C.

**Darkroom**

Blackout you're room so your baby understands it's time to sleep. Turn off the night light, prefer dark, light-proof curtains. When you wake up from lunch or night sleep, make sure to open the curtains and let the daylight understand the different day and night.

**Massage before sleep**

According to a study, babies and young children fall asleep faster than children who only read a pre-sleep book with a 15-minute pre-sleep massage. Open the cover of your baby oil and start the massage with soft touches.

**Daytime sleep**

The contribution of daytime sleep to the mother cannot be ignored. Daytime sleep is very important for your baby's physical and mental development. Do not skip the daytime sleep with the thought of sleeping longer at night, as babies who do not sleep during the day sleep often at night because they are tired.

**Your Voice**

Your baby is learning your voice while you're still in your belly. A quiet tone of sleep, a slow lullaby will help him relax. Also, lullabies and songs are effective in lowering the stress level in the baby.

## Author Profile

    Dr. Chevick is an award-winning doctor and Sleep Center Expert !

> **This article originally appeared on [](https://sleepico.com/)** **Sleepico.com**
> For more helpful articles and blogs for authors, visit Sleepico,
> Inc
> Reprinted with permission of [Sleepico Terms of Use.](https://sleepico.com/tou)

**© 2020 Sleepico, Sleepico Publishing, LLC. All rights reserved.**
