---
title: Taking a nap after work, should i do ?
date: '2020-04-18T23:34:37.121Z'
description: Taking a nap after work
cover: './preview.png'
tags: ['taking a nap', 'taking a nap after work', 'featured']
---

Is taking a nap after work beneficial or harmful? What do you think? There may be some of you who are taking a nap after work and find this activity as beneficial. The more important is the ideas of sleep experts...

# Why does the body want to sleep when you come home from work?

There may be many reasons for taking a nap after work. We can list the known causes as follows;

1.  **Not getting enough sleep at night.** This is the most common reason. You should try to have a healthy sleep every day. If you are living sleep disorders, especially falling asleep problems, check our article: https://sleepico.com/how-to-fall-asleep-faster-at-nights/
2.  **Waking up at night**; there can be many causes for this. For example, hyperactivity patients can not sleep deeply at night. If you are waking up at night frequently, it may be good to see a doctor or sleep expert.
3.  **Sleeping and waking up at different times of the day** will push you to continue sleeping without routine.
4.  **Shift jobs** change your sleeping habits in a bad way.
5.  **Sleep apnea**; Because of the poor sleep at night, they suddenly sleep during the day and feel the need for sleep.
6.  **Narcolepsy disease**; we will define deeply after the list.
7.  People who drink alcohol.
8.  In the case of obesity, there is too much desire to sleep.
9.  Diabetes also has an insufficient feeling of sleep.

## \*\*Narcolepsy (Neurological Disease)

Narcolepsy is simply a neurological disease known for many years. Despite having enough sleep at night, the unbearable desire to sleep in the daytime is the most important symptom of narcolepsy.

Somnolence should be a major problem that can cause serious health problems, but, it is often associated with daily work pace and it is not taken seriously. Many patients consider somnolence as normal for daily business life. Also, these people consider it normal to sleep at noon, in front of the TV in the evening, on the road, and even at work or behind the wheel. However, all of these may be symptoms of narcolepsy.

Fatigue and somnolence should not be confused with each other. Fatigue is a condition that occurs with extreme physical activity. After taking some rest, you can unwind. Excessive sleepiness ( somnolence) in narcolepsy is that the person falls asleep and does not resist this situation.

First of all, it is useful to describe some terms related to narcolepsy. Somnolence in narcolepsy syndrome is a desire to sleep and unbearable sleepiness that comes suddenly and emerges during the day when the person does not feel any sleepiness. The patient may live sleep attacks more often in environments suitable for sleep as well as in inappropriate environments such as while driving.

Fatigue and desire to sleep increase especially in spring. Do the sleep attacks increase in spring in narcolepsy cases? The sleepiness of narcolepsy patients is not affected by any external factors, so it is not affected by seasonal changes.

**_When to consult a sleep expert?_**

Narcolepsy is not a disease that can be managed without consulting a doctor. The problem is that patients are unable to accurately define the symptoms. You should get help from a doctor to define your symptoms correctly.

**Can you specify the treatment methods of narcolepsy?**

Narcolepsy is a disease that can be treated for many years. With this perspective, it is important to diagnose correctly, like all other sleep diseases. In recent years, the treatment alternatives have increased, and with modern medicines, treatment has begun more successfully and safely.

**Is it possible to be protected or prevented?**

Narcolepsy is not a preventable disease. It is necessary to prevent the patients from being affected by this disease at a young age by treating them without losing any time.

# Taking a Nap After Work or in the Afternoon

For many people, the worst part of business life is waking up early in the morning. No matter how much you sleep, it is not enough. Even though you can stand with coffee in the morning, what about the afternoon and evening? Let's check the daily sleeping habits.

**The body wants to sleep 2 times a day**

The study is from the University of Adelaide, Australia. Sleepiness after lunch was extremely normal. The reason is very simple. Although one-third of our lives is asleep, this is not a correct system. The structure of human beings is in kindergartens or kindergartens; Programmed to sleep 2 times a day. He wants to sleep when the time comes. It's also a problem when you don't give it to the body.

**Taking a nap in the afternoon will increase your efficiency**

As you know, the biggest technology companies have been started to put sleep pods into the offices to increase their employees. With this opportunity, employees can sleep in the afternoon or at a permitted time and increase their efficiency and productivity. Taking a nap in the afternoon has great effects on the body. For example, because of the stress hormone called corticosterone increases, our motor skills are negatively affected.

**If possible take a nap at 14:30**

What is the best time to take a nap in the day time? The researcher Dr. According to Fiona Kerr, lunch is a must. It is necessary to 'take a nap' for 15-20 minutes at 14:30. You can think like restarting the computer ... It makes the brain run faster. With taking a nap, stress decreases while motor skills increase.

**The ideal time for a nap: 20 or 90 minutes of sleep**

It is necessary to find a quiet, dark place in the company or at home. Then, close the eyes and focus on breathing for 15-20 minutes. "It works even if you don't fall asleep," says Dr. Kerr. "It's like deleting mail in your email box" he explains. If you wake up during REM, you still have high levels of melatonin which makes you sleepiness. If you wake up during non-REM sleep, your blood pressure and brain activity are slowed down which helps you feel awake. That's why you should wake up before REM sleep to not feel tired.

## Companies where let employees taking a nap.

**Uber**

You can find nap rooms which were designed by Studio O + A in UBER office (headquarter in San Francisco). With this opportunity, UBER employees can take a nap during the day.

**Google**

Google is another company that says ok for a nap. In Google's office, you can find nap pods, free food and drinks, and shower rooms. Sounds great?

**Some others;**

- Mercedes-Benz Financial Services (Fort Worth, TX)
- National Aeronautics and Space Administration (NASA) (Palmdale, California)
- Zappos
- Capital One Labs (San Francisco)
- Ben & Jerry's
- PwC

**_Shift workers, whose sleep patterns are broken, become sick more often. (SOURCE: ONS -_ https://www.ons.gov.uk )**

Shift work is correlated with many health problems. Researchers show that workers who sleep less and sleep during the wrong time of day increase their risk of diabetes and obesity.

According to the research of ONS, you can see the correlation as explained with the below chart.

![Does the constant change of working hours cause disease](https://sleepico.com/img/does-the-constant-change-of-working-hours-cause-disease.jpg)

# 5 Nap Hacks for a Productive Day

**1. Regarding the research of the University of California in 2010, the ideal time to take a nap is after lunch.**

It is well-known information that one and a half hours of a nap at noon deletes memories in short-term memory and creates space for new information.

**2. Find a quiet and dark place for a good sleep**

If you want to have a good nap, you should find an as much as quiet and dark place. If your company has sleep pods, it is good for you. Otherwise, you should check the available places or ask your company to buy a sleep pod.

**3. Set an alarm before nap**

A nap during the day for 10-20 minutes increases your energy because you have not yet fallen into REM sleep. If you wake up in REM sleep, you will be more tired and fizzy. That's why the setting alarm is very important if you are plan taking a nap.

**4. Adapt your room temperature with your body temperature**

When you take a nap, your body temperature decreases, that's why taking a nap in a fresh room will support your body to sleep adapting to your body's natural tendency. You can consider the ideal temperature for sleeping between 15 - 19 ºC.

**5. Drink a cup of coffee before taking a 20 minutes nap**

It is a good idea to drink a cup of coffee before taking a nap. Don't be worried, it takes 20 minutes to see the effect of caffeine; so if you decide to take a nap after drinking a cup of coffee, caffeine in the coffee will help you wake up and let you have an efficient afternoon.

# What are the Effects of Nap on the Brain?

There are many questions that we ask frequently. We can exemplify them as following; Is taking a nap good for your health? Is napping for 2 hours bad? Are 10-minute naps good for you and your brain? Is it normal to take a nap every day? There are lots of questions and here below you can find the answer to the effects on the brain.

Parents especially know the importance of taking a nap in the afternoon for their children. Taking a nap is very useful for the healthy functioning of the brain, but do we know the ideal duration for a nap?

Our cells are renewed and healed while our body is in a sleep cycle. **If we don't get enough sleep during the day,** this sleeplessness will damage our immune system, making us weak against diseases. On the other hand, enough sleep fights several types of cancer, protects our cardiovascular health, desensitize, and improves our metabolism and also provides weight control.

## **The benefits of taking a nap in the afternoon**

In the modern world, it is not easy to take a nap during the day. There are very few companies that let their employees take a nap in the afternoon. Scientific researches say that the nap in the afternoon is very beneficial for our brain and brings many advantages to our life accordingly to our companies. If you can't sleep enough at night, a short nap in the afternoon improves mental alertness and motor skills. Not only improves response time but also improves our mood.

## **How long should you nap?**

First of all, must be informed that too much sleep is harmful. If you take a nap to improve your daily productivity, you should be careful while planing the time and duration of the nap.

- If you feel just tired in the afternoon, it will be enough for you to take a nap for 10-20 minutes.
- If you feel angry or stressed, if you want to remember some important parts from the book you read, a 60-minute nap has been proven to reduce impulsivity, improve anger tolerance, and help remember.
- If you feel exhausted, you need a 90-minute nap to reset and renew yourself. 90 minutes does not only improve your creativity, emotional and systematic memory; it also reduces the possibility of drowsiness.

## **What's the best time of the day to nap?**

You can consider 13:00 as the best hour for those who wake up early. For night owls, 15:00 is a good time. Keep in mind that you should never sleep after 16:00. Sleeping or taking a nap after 16:00 is unhealthy and also puts night sleep at risk.

**Bonus Info**

Before taking a 20-minute nap, drink a cup of coffee and find a place dark and quiet. You will wake up rested and renewed.

# Importance of Taking a Nap Before Making an Important Decision

Don't forget that our biological clock is programmed for long sleep at night and short sleep at daylight. In the past, it was a very common habit to take a nap during the day. Nowadays, it seems like a luxury or a lazy habit but the truth, it is the necessity of our life.

When you feel exhausted in the middle of the day, you can take a short break or take a 20-minute nap and then put your work in a more sufficient way when you wake up. Remember, only 20 minutes is enough for a nap...

**Why do we need a nap?**

From the moment we wake up, our body begins to prepare us to sleep again. As we stay awake, sleep pressure and desire for deep sleep increase, and in the end, we eventually fall asleep where we are. If you have not slept well at night, a nap in the afternoon is a must for you.

There is misinformation that when we have a nap during the day, our sleep at night is negatively affected. Nap during the day does not affect your night sleep, it also makes it easier to fall asleep at night.

**The benefits of confectionery:**

- Our focus on work and our productivity increases
- Support us to make more accurate and clear decisions
- Strengthens our attention and memory
- Nap makes us look young, you can consider as a real beauty sleep
- Regulates our sexual life
- Even many people think the opposite, nap lets us lose weight
- Reduces the risk of heart attack and diabetes
- Increases our body resistance and strength
- Reduces stress and anxiety **_(this is one of the most important points while making an important decision)_**

**How long can you take a nap?**

If you take a 20-minute nap, you can consider the first 10 minutes is the relaxation period. Our pulse and our body temperature decrease. A short 20-minute nap improves attention, concentration, and mood. Ideally, a 90-minute nap let you live deep sleep and REM sleep, which improves and maximize your creativity. So, it is up to the time you have. A 20-minute nap or a 90-minute nap...

**Difference between a 20-minute nap and a 90-minute nap**

If you need the energy to do physical work, just a 20-minute nap will be enough for you. If you are about to take a very important decision or if you are going to do original and unique work, keep your sleep for 90 minutes. Keep in mind that the best time for a nap is between 13:00 and 15:00.

Many of you are saying, "I can't find time for a nap during the day". If you find time to drink a coffee for 20 minutes after a lunch break, you can also find time to take a nap.

**Suggestions before taking a nap;**

- Put away everything about your jobs and responsibilities
- Turn off your phone, find a quiet and dark place
- Protein foods such as cheese, nuts, and peanuts increase melatonin, making it easier to take a nap.
- Drinking coffee for a 20-minute nap is a good idea to wake up more energic. Note: don't drink coffee for a 90-minute nap. The reason already explained above of the article.

# Is napping good for your heart?

This is another most discussed question about napping. Is taking a nap good for your heart? The short answer is yes. Let's continue with details.

According to researchers of sleep experts, a 20-minute nap should be allowed during the day. A nap in the afternoon affects mental and physical performance positively.

A 20-minute nap between 13:00 and 15:00 protects heart health. Because of this, an opportunity should be created for taking a nap in business life. Sleep experts are thinking that the number of the company that let their employees taking a nap in the afternoon, will increase. A 20-minute nap is very important not only for the heart but also for brain and vascular health.

**Does the ideal sleep time differ from person to person?**

Neurology and Pain Specialists say that a 20-minute nap improves afternoon performance. There are two periods when our body's metabolism decreases. One is in the morning and the other is in the afternoon. Especially between 13.00-15.00 after eating. This is one of the best times to fall asleep."

The duration of the nap should be planned in a way that will not stop working life. While taking a nap, cool and quiet places should be preferred. Sleep experts also state that a nap is not a backup for night sleep. Bonus info: The sleep time for a healthy body should not be less than 6 hours.

# Siesta Culture in Spain

A siesta is a short nap taken in the early afternoon (14:00 - 15:00), mostly after lunch. Such a nap is a common tradition in some countries, especially those where the weather is hot. Nowadays, it has started to be used to the leading technology companies in the world. Why is it useful to have a nap in the afternoon? What are the disadvantages of sleeplessness? How long should a short nap be in the afternoon? Are extreme naps at noon harmful? You will find the answer to your questions here below.

**Why is it useful to have a nap in the afternoon?**

Do you know the opportunities of people who work in companies like **_NASA, Google, Samsung, Apple, Microsoft_** or other big technology companies? All of them can take a short nap in nap pods specially designed for them during business hours. Engineers, programmers, and astronauts can reach such a nap pod and take a short 20-minute nap.

Many technology companies have appeared that manufacture nap pods. MetroNaps is one of them. The MetroNaps company in New York is not only producing nap pods for technology companies but also producing for lawyers, management consulting companies and airlines.

Manager of MetroNaps, Christopher Lindholst, take attention that people from every branch of business that tries to get and hire qualified personnel can benefit from this nap pod. Win-win situation... Employees want to work for such an open-minded company. Companies want to hire high qualified employees and get high efficiency from them in this way.

**What are the disadvantages of sleeplessness?**

In recent research published in Nature Medicine (journal), it is figured out that insomnia is connected with many health-related issues, including obesity, diabetes, stroke, heart attack, etc.

As part of the study managed by neuroscience and psychology specialists from the universities of the USA, France, and Israel, it was reviewed how the human brain responded when sleep deprived. If the person spent one night without sleep, his brain could not function accurately. The process of turning visual input into intelligent thinking in the brain took a long time.

Sleeping behind the wheel is another problem for guys who sleep less than they need. Let's imagine a life-and-death situation. A person walking on the road suddenly gets in front of your car. Seconds have an extraordinary effect on this situation.

**The benefits of confectionery**

The best example of taking a nap in the afternoon should be the Spanish siesta application. Researchers of the Sociedad Española de Médicos de Atención Primaria (SEMERGEN) advised that insomnia had a much worse effect than being hungry for employees to keep the same productivity in the afternoon.

The judgments from this study were verified by another study handled in China with people aged 65 and over who took a short nap between 13:00 and 15:00 developed cognitive skills.

Other studies also show that taking a nap not only improves cognitive skills but also reduces getting heart disease.

The author of the book "Take a Nap! Change Your Life." Sara Mednick goes even further on the benefits of taking nap, that these naps trigger agility, improves creativity, relieve tension, endurance, and motor skills, reduces the possibility of wrong decisions and acts, positively affect sexual life, make healthier decisions. She suggests that it supports the person to look younger, lose weight (answer to one of the most written questions: Does afternoon nap increase weight?), improve emotional mood and strengthen memory.

**Is it bad to take a nap every day?**

It is up to how you practice in your life. If you are taking a 20-minutes nap in the afternoon every day, there is no problem. But if you are taking a long nap out of the hours between 13:00 - 15:00, it may be harmful. Is a 2-hour nap too long? Yes, it is long and if you take a 2-hour nap, most probably you will wake up more tired.

**Is it possible to live without a nap, without sleep?**

First of all, you need to understand how the system is working. The Biological clock commands the person with a 24-hour sleep/wake cycle, sleep desire and forced to sleep. When the person exceeds the time in the biological clock, the brain begins to produce more adenosine. Adenosine chemical regularly dictates the necessity of sleeping by sending signals that increase the desire to sleep and reach the top level after 16 hours.

> **Bonus info:** Caffeine blocks the adenosine receptors in the brain
> and keeps us awake. After the end of the caffeine effect, the desire
> to sleep shows itself again.

Do you want to hear interesting information about sleeplessness? When we stay awake for 24 hours or more, the level of cognitive impairment in our blood reaches the same level with the presence of 0.1 percent alcohol in our blood. You know in some countries even exceeds the alcohol limit in traffic...

In a study, the participants slept only four hours a day for six days. Do you imagine the results? What they found from the participants are high blood pressure, high cortisol (stress hormone) level and insulin resistance, which is a diabetes type 2 precursor. It is not possible to live without sleep. Let's check what happens step by step if we stay awake.

# Is it possible to survive without sleep? How long can we stay awake?

## What happens after 24 hours of no sleep?

You will start to live below conditions;

- Losing good judgment
- Problems at taking a decision
- A decrease in hand-eye coordination
- Increase the risk of death from an accident

## What happens after 48 hours of no sleep?

**After 48 hours**, you will become very fatigued. Then, unconsciousness will start and there will be seen microsleep.

## What happens after 72 hours of no sleep?

After **72 hours of no sleep** can put your mental and motor skills at a higher risk. You will place yourself in the middle of awakening and sleeping. After 72 hours there may be seen mental problems that may occur physical problems in daily life.

# Summary of the article, Taking a Nap After Work or in the Afternoon. Should I Do?

> We all have to sleep to have both a healthy life. Sleeping has huge
> effects on our mental and physical health. If we plan our sleeping
> times very well, we can see the results in a couple of days. Napping
> is a very healthy activity for all of us. If you have the opportunity
> to take a nap during the day (between 13:00-15:00) don't miss this
> opportunity. You will see an increase in your productivity and
> efficiency. Don't push yourself to get this habit just try. On the
> other hand, don't take a nap over 90 minutes which brings harmful and
> this is just the opposite that we aim.
>
> You can ask your company to let you take a nap in the afternoon. Don't
> worry about this, a 20-minute nap doesn't damage your company if the
> process in your company is compatible with this application. just
> opposite, you and your company will see the benefits of this napping
> treatment.

Please don't forget to write down your comments, your experiences in your company or daily life into below comment area.

## Author Profile

    Dr. Addy is an award-winning doctor and Sleep Center Expert !

> **This article originally appeared on [](https://sleepico.com/)** **Sleepico.com**
> For more helpful articles and blogs for authors, visit Sleepico,
> Inc
> Reprinted with permission of [Sleepico Terms of Use.](https://sleepico.com/tou)

**© 2020 Sleepico, Sleepico Publishing, LLC. All rights reserved.**
