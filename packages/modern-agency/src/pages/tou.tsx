import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Tou from "../containers/Tou"

type TouPageProps = {}

const TouPage: React.FunctionComponent<TouPageProps> = props => {
  return (
    <Layout>
      <SEO
        title="Terms Of Use"
        description="Privacy Policy, Terms of Use, Disclaimer"
      />

      <Tou />
    </Layout>
  )
}

export default TouPage
