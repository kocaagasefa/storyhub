import * as React from "react"
import { TouWrapper, TouPageTitle, TouDetails } from "./style"

interface TouProps {}

const Tou: React.FunctionComponent<TouProps> = props => {
  return (
    <TouWrapper>
      <TouDetails>
        <h2>Privacy Policy </h2>
        <p>
          Sleepico Experts built the Sleepico.com app as a Free available blog.
          This SERVICE is provided by Sleepico Experts at no cost and is
          intended for use as is. This page is used to inform visitors regarding
          my policies with the collection, use, and disclosure of Personal
          Information if anyone decided to use my Service. If you choose to use
          my Service, then you agree to the collection and use of information in
          relation to this policy. The Personal Information that I collect is
          used for providing and improving the Service. I will not use or share
          your information with anyone except as described in this Privacy
          Policy. The terms used in this Privacy Policy have the same meanings
          as in our Terms and Conditions, which is accessible at Sleepico.com
          unless otherwise defined in this Privacy Policy. Information
          Collection and Use For a better experience, while using our Service, I
          may require you to provide us with certain personally identifiable
          information. The information that I request will be retained on your
          device and is not collected by me in any way. The app does use third
          party services that may collect information used to identify you. Link
          to privacy policy of third party service providers used by the app
          Google Play Services Log Data I want to inform you that whenever you
          use my Service, in a case of an error in the app I collect data and
          information (through third party products) on your phone called Log
          Data. This Log Data may include information such as your device
          Internet Protocol (“IP”) address, device name, operating system
          version, the configuration of the app when utilizing my Service, the
          time and date of your use of the Service, and other statistics.
          Cookies Cookies are files with a small amount of data that are
          commonly used as anonymous unique identifiers. These are sent to your
          browser from the websites that you visit and are stored on your
          device's internal memory. This Service does not use these “cookies”
          explicitly. However, the app may use third party code and libraries
          that use “cookies” to collect information and improve their services.
          You have the option to either accept or refuse these cookies and know
          when a cookie is being sent to your device. If you choose to refuse
          our cookies, you may not be able to use some portions of this Service.
          Service Providers I may employ third-party companies and individuals
          due to the following reasons: To facilitate our Service; To provide
          the Service on our behalf; To perform Service-related services; or To
          assist us in analyzing how our Service is used. I want to inform users
          of this Service that these third parties have access to your Personal
          Information. The reason is to perform the tasks assigned to them on
          our behalf. However, they are obligated not to disclose or use the
          information for any other purpose. Security I value your trust in
          providing us your Personal Information, thus we are striving to use
          commercially acceptable means of protecting it. But remember that no
          method of transmission over the internet, or method of electronic
          storage is 100% secure and reliable, and I cannot guarantee its
          absolute security. Links to Other Sites This Service may contain links
          to other sites. If you click on a third-party link, you will be
          directed to that site. Note that these external sites are not operated
          by me. Therefore, I strongly advise you to review the Privacy Policy
          of these websites. I have no control over and assume no responsibility
          for the content, privacy policies, or practices of any third-party
          sites or services. Children’s Privacy These Services do not address
          anyone under the age of 13. I do not knowingly collect personally
          identifiable information from children under 13. In the case I
          discover that a child under 13 has provided me with personal
          information, I immediately delete this from our servers. If you are a
          parent or guardian and you are aware that your child has provided us
          with personal information, please contact me so that I will be able to
          do necessary actions. Changes to This Privacy Policy I may update our
          Privacy Policy from time to time. Thus, you are advised to review this
          page periodically for any changes. I will notify you of any changes by
          posting the new Privacy Policy on this page. These changes are
          effective immediately after they are posted on this page. Contact Us
          If you have any questions or suggestions about my Privacy Policy, do
          not hesitate to contact me at mail@sleepico.com.
        </p>
      </TouDetails>

      <TouDetails>
        <h2>Disclaimer</h2>
        <p>
          If you require any more information or have any questions about our
          site's disclaimer, please feel free to contact us by email at
          mail@sleepico.com Disclaimers for Sleepico, Inc All the information on
          this website - sleepico.com - is published in good faith and for
          general information purpose only. Sleepico, Inc does not make any
          warranties about the completeness, reliability and accuracy of this
          information. Any action you take upon the information you find on this
          website (Sleepico, Inc), is strictly at your own risk. Sleepico, Inc
          will not be liable for any losses and/or damages in connection with
          the use of our website. Our Disclaimer was generated with the help of
          the Disclaimer Generator and the Disclaimer Generator. From our
          website, you can visit other websites by following hyperlinks to such
          external sites. While we strive to provide only quality links to
          useful and ethical websites, we have no control over the content and
          nature of these sites. These links to other websites do not imply a
          recommendation for all the content found on these sites. Site owners
          and content may change without notice and may occur before we have the
          opportunity to remove a link which may have gone 'bad'. Please be also
          aware that when you leave our website, other sites may have different
          privacy policies and terms which are beyond our control. Please be
          sure to check the Privacy Policies of these sites as well as their
          "Terms of Service" before engaging in any business or uploading any
          information. Consent By using our website, you hereby consent to our
          disclaimer and agree to its terms. Update Should we update, amend or
          make any changes to this document, those changes will be prominently
          posted here.
        </p>
      </TouDetails>
      <TouDetails>
        <h2>Terms of Use</h2>
        <p>
          Introduction These Website Standard Terms and Conditions written on
          this webpage shall manage your use of our website, Sleepico accessible
          at sleepico.com. These Terms will be applied fully and affect to your
          use of this Website. By using this Website, you agreed to accept all
          terms and conditions written in here. You must not use this Website if
          you disagree with any of these Website Standard Terms and Conditions.
          These Terms and Conditions have been generated with the help of the
          Terms And Conditions Template and the Terms and Conditions Generator.
          Minors or people below 18 years old are not allowed to use this
          Website. Intellectual Property Rights Other than the content you own,
          under these Terms, Sleepico, Inc and/or its licensors own all the
          intellectual property rights and materials contained in this Website.
          You are granted limited license only for purposes of viewing the
          material contained on this Website. Restrictions You are specifically
          restricted from all of the following: publishing any Website material
          in any other media; selling, sublicensing and/or otherwise
          commercializing any Website material; publicly performing and/or
          showing any Website material; using this Website in any way that is or
          may be damaging to this Website; using this Website in any way that
          impacts user access to this Website; using this Website contrary to
          applicable laws and regulations, or in any way may cause harm to the
          Website, or to any person or business entity; engaging in any data
          mining, data harvesting, data extracting or any other similar activity
          in relation to this Website; using this Website to engage in any
          advertising or marketing. Certain areas of this Website are restricted
          from being access by you and Sleepico, Inc may further restrict access
          by you to any areas of this Website, at any time, in absolute
          discretion. Any user ID and password you may have for this Website are
          confidential and you must maintain confidentiality as well. Your
          Content In these Website Standard Terms and Conditions, "Your Content"
          shall mean any audio, video text, images or other material you choose
          to display on this Website. By displaying Your Content, you grant
          Sleepico, Inc a non-exclusive, worldwide irrevocable, sub licensable
          license to use, reproduce, adapt, publish, translate and distribute it
          in any and all media. Your Content must be your own and must not be
          invading any third-party’s rights. Sleepico, Inc reserves the right to
          remove any of Your Content from this Website at any time without
          notice. Your Privacy Please read Privacy Policy. No warranties This
          Website is provided "as is," with all faults, and Sleepico, Inc
          express no representations or warranties, of any kind related to this
          Website or the materials contained on this Website. Also, nothing
          contained on this Website shall be interpreted as advising you.
          Limitation of liability In no event shall Sleepico, Inc, nor any of
          its officers, directors and employees, shall be held liable for
          anything arising out of or in any way connected with your use of this
          Website whether such liability is under contract. Sleepico, Inc,
          including its officers, directors and employees shall not be held
          liable for any indirect, consequential or special liability arising
          out of or in any way related to your use of this Website.
          Indemnification You hereby indemnify to the fullest extent Sleepico,
          Inc from and against any and/or all liabilities, costs, demands,
          causes of action, damages and expenses arising in any way related to
          your breach of any of the provisions of these Terms. Severability If
          any provision of these Terms is found to be invalid under any
          applicable law, such provisions shall be deleted without affecting the
          remaining provisions herein. Variation of Terms Sleepico, Inc is
          permitted to revise these Terms at any time as it sees fit, and by
          using this Website you are expected to review these Terms on a regular
          basis. Assignment The Sleepico, Inc is allowed to assign, transfer,
          and subcontract its rights and/or obligations under these Terms
          without any notification. However, you are not allowed to assign,
          transfer, or subcontract any of your rights and/or obligations under
          these Terms. Entire Agreement These Terms constitute the entire
          agreement between Sleepico, Inc and you in relation to your use of
          this Website, and supersede all prior agreements and understandings.
          Governing Law & Jurisdiction These Terms will be governed by and
          interpreted in accordance with the laws of the State of tr, and you
          submit to the non-exclusive jurisdiction of the state and federal
          courts located in tr for the resolution of any disputes.
        </p>
      </TouDetails>
    </TouWrapper>
  )
}

export default Tou
