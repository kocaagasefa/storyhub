import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Contact from "../containers/Contact"

type ContactPageProps = {}

const ContactPage: React.FunctionComponent<ContactPageProps> = props => {
  return (
    <Layout>
      <SEO
        title="Contact Us"
        description="Don't be a stranger. Just say hello to our sleep experts. Feel free to get in touch with our sleep experts."
      />

      <Contact />
    </Layout>
  )
}

export default ContactPage
